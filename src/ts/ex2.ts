interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
  }
   
  const questions: Question[] = [
    {
      question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
      choices: [
        '"object"', 
        '"null"', 
        '"undefined"', 
        '"boolean"'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to add one or more elements to the end of an array?',
      choices: ['push()', 'join()', 'slice()', 'concat()'],
      correctAnswer: 0,
    },
    {
      question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
      choices: ['"327"', '"12"', '"57"', '"NaN"'],
      correctAnswer: 2,
    },
    {
      question: 'What is the purpose of the "use strict" directive in JavaScript?',
      choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
      correctAnswer: 2,
    },
    {
      question: 'What is the scope of a variable declared with the "let" keyword?',
      choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
      correctAnswer: 2,
    },
    {
      question: 'Which higher-order function is used to transform elements of an array into a single value?',
      choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
      correctAnswer: 2,
    },
    {
      question: 'What does the "=== " operator in JavaScript check for?',
      choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
      correctAnswer: 1,
    },
    {
      question: 'What is the purpose of the "this" keyword in JavaScript?',
      choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
      correctAnswer: 3,
    },
    {
      question: 'What does the "NaN" value represent in JavaScript?',
      choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to remove the last element from an array?',
      choices: ['pop()', 'shift()', 'slice()', 'splice()'],
      correctAnswer: 0,
    },
  ];
  const divv = document.getElementById('ex22') as HTMLDivElement;
  const scoreText = document.createElement('p');
  let score = 0;
  scoreText.textContent = `Current Score: ${score}/10`;
  document.body.insertBefore(scoreText, document.body.firstChild);
  
  const submitButtons: HTMLButtonElement[] = [];
  const radioButtons: HTMLInputElement[] = [];

  for (let i = 0; i < questions.length; i++) {
    const question = questions[i];
    const paragraph = document.createElement('p');
    paragraph.textContent = question.question;
    document.body.appendChild(paragraph);
  
    const choicesContainer = document.createElement('div');
    choicesContainer.classList.add('choices-container');
    document.body.appendChild(choicesContainer);
    const radioButtons: HTMLInputElement[] = [];
    for (let j = 0; j < question.choices.length; j++) {
      const choiceContainer = document.createElement('div');
      choiceContainer.classList.add('choice-container');
      choicesContainer.appendChild(choiceContainer);
  
      const radio = document.createElement('input');
      radio.type = 'radio';
      radio.name = `question-${i}`;
      radio.value = j.toString();
  
      const label = document.createElement('label');
      label.textContent = question.choices[j];
      label.insertBefore(radio, label.firstChild);
  
      choiceContainer.appendChild(label);
      radioButtons.push(radio);
    }
  
    const submit = document.createElement('button');
    submit.textContent = 'Submit';
    const br = document.createElement('br');
    choicesContainer.appendChild(br);
    choicesContainer.appendChild(submit);
  
    submitButtons.push(submit);
  
    submit.addEventListener('click', function () {
      const selectedRadio = document.querySelector(`input[name="question-${i}"]:checked`) as HTMLInputElement;
  
      if (selectedRadio) {
        const selectedChoiceIndex = parseInt(selectedRadio.value);
        radioButtons.forEach((radio) => {
          radio.disabled = true;
        });
        if (selectedChoiceIndex === question.correctAnswer) {
          score++;
        }
  
        scoreText.textContent = `Current Score: ${score}/10`;
  
        
        
          submit.disabled = true;
          radioButtons.forEach((radio) => {
            radio.disabled = true;
          });
      }
    });
  }
    // for (let i = 0; i <1; i++) {
    //   const br = document.createElement('br')
    //   const submit = document.createElement('button');
    //   const scoretext = document.createElement('p');
    //   submit.textContent='Submit'
    //   let score = 0;
    //   document.body.appendChild(submit);
    //   submit.appendChild(br)
    //   submit.addEventListener("click",function(){
    //     const selectchoice  = document.querySelector('input[name=question${i}]:checked') as HTMLInputElement
        
    //     if (selectchoice){
    //       const selectchoiceindex = parseInt(selectchoice.value)
          
    //       if(selectchoiceindex == question.correctAnswer){
            
    //         score++
            
    //         scoretext.textContent= "Current Score:" + score + "/10"
    //         document.body.appendChild(scoretext);
    //       }
    //     }
    //   })
    // }
  
    
 // }
  


  

  // for (const Question of questions){
  //   const paragraphq = document.createElement('p');
  //   paragraphq.textContent = Question.question;
  //   document.body.appendChild(paragraphq);
   
    
  //   if(Question.choices.length >0){
  //     const paragraphq = document.createElement('p');
  //     for (const choices of Question.choices){
  //   const choice = document.createTextNode(choices)
  //   const label = document.createElement('label');
  //   const radio = document.createElement('input');
  //   radio.type = 'radio';
  //   radio.name = 'option';
  //   radio.appendChild(choice)
  //   paragraphq.appendChild(radio)
  //   }
  //   paragraphq.appendChild(paragraphq)
  //   }

  // }
// }