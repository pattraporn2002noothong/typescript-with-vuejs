
// const inputnumber = document.getElementById('Input Number') as HTMLInputElement
// const inputnumber2 = document.getElementById('Input Number2') as HTMLInputElement
// const equalButton = document.getElementById('equalButton')as HTMLButtonElement
// const notequalButton = document.getElementById('notequalButton')as HTMLButtonElement
// const greaterThanButton = document.getElementById('greaterThanButton')as HTMLButtonElement
// const greaterThanorEqualButton= document.getElementById('greaterThanorEqualButton')as HTMLButtonElement
// const output = document.getElementById('output')as HTMLParagraphElement


// equalButton.addEventListener('click',function(){
//     const value:number = +inputnumber.value
//     const value2:number = +inputnumber2.value
//     const result = value ==  value2
//     output.innerText = result+''
//     if (result == true){
//         output.style.color = 'green'
//     }else{
//         output.style.color = 'red'
//     }
    
// })
// notequalButton.addEventListener('click',function(){
//     const value:number = +inputnumber.value
//     const value2:number = +inputnumber2.value
//     const result = value !=  value2
//     output.innerText = result+''
//     if (result == true){
//         output.style.color = 'green'
//     }else{
//         output.style.color = 'red'
//     }
// })
// greaterThanButton.addEventListener('click',function(){
//     const value:number = +inputnumber.value
//     const value2:number = +inputnumber2.value
//     const result = value >  value2
//     output.innerText = result+''
//     if (result == true){
//         output.style.color = 'green'
//     }else{
//         output.style.color = 'red'
//     }
// })
// greaterThanorEqualButton.addEventListener('click',function(){
//     const value:number = +inputnumber.value
//     const value2:number = +inputnumber2.value
//     const result = value >=  value2
//     output.innerText = result+''
//      if (result == true){
//         output.style.color = 'green'
//     }else{
//         output.style.color = 'red'
//     }
// })
// export { }

//////////////////////////////////////////////
interface Address{
    street: string
    city: string
    country: string
}
interface Person{
    name: string
    age: number
    address:Address
    hobbies:string[]
}
const n:number = 10
const s:string = 'text'
const b:boolean = true
// const array:number[]=[1,2,3]


 const person: Person = {
    name: 'John',
    age: 25,
    address:{
        street:'123 Main Street',
        city: 'Chicago',
        country: 'USA'
    },
    hobbies:['coding,phone']
 }
let address:Address={
    street:'123 Main Street',
    city: 'Chicago',
    country: 'USA'
 }
 person.address=address

function add(number1:number,number2:number):number{
    return number1+number2
}
function DisplayDetail(person:Person):string{
    return 'name='+person.name+'; age='+person.age+
    "; address="
    +person.address.street
    +person.address.city
    +person.address.country
    +'; hobbies='+person.hobbies.join(',')
}
console.log(DisplayDetail({
    name:'เนย',
    age: 21 ,
    address:{
        city:'โลก',
        country:'ไทย',
        street:'ชลบุรี',
    },
    hobbies:['นอน','นอน']
}))
const addresses: Address[]=[
    {
        city:'โลก',
        country:'ไทย',
        street:'ชลบุรี',
    },
    {
        city:'โลก',
        country:'ไทย',
        street:'ชลบุรี',
    }
    ,
    {
        city:'โลก',
        country:'ไทย',
        street:'ชลบุรี',
    },
    {
        city:'โลก',
        country:'ไทย',
        street:'ชลบุรี',
    }
]
const sample = {
    a: 'A',
    b: 'B'
}
 export { }